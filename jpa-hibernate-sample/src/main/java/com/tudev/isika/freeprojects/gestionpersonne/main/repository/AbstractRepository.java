package com.tudev.isika.freeprojects.gestionpersonne.main.repository;

import java.util.logging.Logger;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.tudev.isika.freeprojects.gestionpersonne.main.utils.HibernateUtil;

/**
 * Classe abstraite utilisée pour définir les méthodes communes des différents
 * composants qui implémentent {@link IRepository}.
 */
public abstract class AbstractRepository {

	/**
	 * Retourne le logger spécifique à une classe fille de
	 * {@link AbstractRepository}.
	 * 
	 * @return {@link Logger}
	 */
	public abstract Logger getLogger();

	/**
	 * Renvoie une nouvelle session hibernate.
	 * 
	 * @return {@link Session}
	 */
	public Session openSession() {
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
		} catch (HibernateException e) {
			// Get logger permet d'appeler le logger des classes filles de
			// AbstractRepository.
			getLogger().severe("Erreur survenue lors de la création de la session : " + e);
		}
		return session;
	}

	/**
	 * Ferme la session indiquée en entrée.
	 * 
	 * @param session
	 */
	public void closeSession(Session session) {
		try {
			if (session != null) {
				session.close();
			}
		} catch (HibernateException e) {
			// Get logger permet d'appeler le logger des classes filles de
			// AbstractRepository.
			getLogger().severe("Erreur survenue lors de la fermeture de la session : " + e);
		}
	}

}
