package com.tudev.isika.freeprojects.gestionpersonne.main.utils;

import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

/**
 * Classe utilitaire qui permet d'initialiser Hibernate et la
 * {@link SessionFactory}.<br>
 * C'est une classe d'aide qui facilite l'utilisation de Hibernate.<br>
 * Ce n'est pas utilisable en l'état pour un vrai projet mais pour faire des
 * exemples rapides ça fait l'affaire.
 * 
 * @author Mohamed
 */
public class HibernateUtil {

	private static final Logger LOGGER = Logger.getLogger(HibernateUtil.class.getSimpleName());
	
	/**
	 * Registre des services hibernate
	 */
	private static StandardServiceRegistry registry;

	/**
	 * Fabrique de session hibernate
	 */
	private static SessionFactory sessionFactory;

	/**
	 * Retourne une instance statique de la {@link SessionFactory} hibernate, qu'on
	 * utilise pour créer des sessions.
	 * 
	 * @return {@link SessionFactory}
	 */
	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			try {
				// Create registry
				registry = new StandardServiceRegistryBuilder().configure().build();

				// Create MetadataSources
				MetadataSources sources = new MetadataSources(registry);

				// Create Metadata
				Metadata metadata = sources.getMetadataBuilder().build();

				// Create SessionFactory
				sessionFactory = metadata.getSessionFactoryBuilder().build();

			} catch (Exception e) {
				LOGGER.severe("Erreur survenue lors de la création de la session factory : " + e.getMessage());
				if (registry != null) {
					StandardServiceRegistryBuilder.destroy(registry);
				}
			}
		}
		return sessionFactory;
	}

	/**
	 * Permet d'arrêter hibernate et de fermer les ressources (SessionFactory, ServiceRegistry)
	 */
	public static void shutdown() {
		if (registry != null) {
			StandardServiceRegistryBuilder.destroy(registry);
		}
	}
}
