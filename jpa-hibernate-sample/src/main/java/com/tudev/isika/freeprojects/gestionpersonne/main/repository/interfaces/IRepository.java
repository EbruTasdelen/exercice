package com.tudev.isika.freeprojects.gestionpersonne.main.repository.interfaces;

import java.util.List;
import java.util.Optional;

import com.tudev.isika.freeprojects.gestionpersonne.main.model.Personne;

/**
 * Interface de marquage des classes représentant les dépôts d'entités métier.
 * 
 * @author Mohamed
 *
 * @param <E> Type générique de l'entité gérée.
 */
public interface IRepository<E> {

	/**
	 * Persiste l'entité indiquée en entrée.<br>
	 * [Création d'une nouvelle ligne <b>=> SQL : INSERT INTO ... </b>].
	 * 
	 * @param entite Entité à persister.
	 * @return <b> Optional<Long> : id de l'entité</b> après persistance (affecté par hibernate)é.
	 */
	Optional<Long> persister(E entite);

	/**
	 * Met à jour l'entité indiquée en entrée.<br>
	 * [Mise à jour des colonnes d'une ligne <b>=> SQL : UPDATE ...</b>].<br>
	 * <br>
	 * <b>N.B : </b>Si l'entité n'existe pas, elle sera persistée.
	 * 
	 * @param entite : Entité à mettre à jour.
	 * @return Optional<E> : entité après persistance.
	 */
	Optional<Personne> fusionner(E entite);

	/**
	 * Supprime l'entité indiquée en entrée.<br>
	 * [Suppression d'une ligne <b>=> SQL : DELETE FROM ...</b>].<br>
	 * 
	 * @param entite: Entité à supprimer.
	 */
	void supprimer(E entite);

	/**
	 * Charger l'entité dont l'identifiant est indiqué en entrée.<br>
	 * [Suppression d'une ligne <b>=> SQL : DELETE FROM ...</b>].<br>
	 * 
	 * @param id : Identifiant de l'entité à charger.
	 * @return Optional<E> : entité correspondant à l'identifiant indiqué en entrée.
	 */
	Optional<Personne> rechercheParId(Long id);

	/**
	 * Charger toutes les entités.
	 * 
	 * @return Liste d'entités (toutes les lignes d'une table)
	 */
	List<E> rechercherToutesLesEntites();
}
