package com.tudev.isika.freeprojects.gestionpersonne.main.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @author Mohamed
 *
 */
@Entity
@Table(name = "ApplicationEntity")
public class ApplicationEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7551408717903575661L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long appId;

	@Column(name = "ApplicationName", length = 255, updatable = true, insertable = true, nullable = true)
	private String applicationName;

	@Column(name = "ApplicationVersion", length = 50, updatable = true, insertable = true, nullable = true)
	private String version;

	public ApplicationEntity() {
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Long getAppId() {
		return appId;
	}

	public void setAppId(Long appId) {
		this.appId = appId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ApplicationEntity [appId=");
		builder.append(appId);
		builder.append(", applicationName=");
		builder.append(applicationName);
		builder.append(", version=");
		builder.append(version);
		builder.append("]");
		return builder.toString();
	}
	
}
