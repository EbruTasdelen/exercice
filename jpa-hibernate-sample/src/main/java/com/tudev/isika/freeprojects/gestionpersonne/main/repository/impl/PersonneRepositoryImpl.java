package com.tudev.isika.freeprojects.gestionpersonne.main.repository.impl;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.tudev.isika.freeprojects.gestionpersonne.main.model.Personne;
import com.tudev.isika.freeprojects.gestionpersonne.main.repository.AbstractRepository;
import com.tudev.isika.freeprojects.gestionpersonne.main.repository.interfaces.IRepository;

/**
 * Repository de gestion des entités <b>{@link Personne}.
 */
public class PersonneRepositoryImpl extends AbstractRepository implements IRepository<Personne> {

	private static final Logger LOGGER = Logger.getLogger(PersonneRepositoryImpl.class.getSimpleName());

	@Override
	public Optional<Long> persister(Personne entite) {

		Long idAffecteParHibernate = null;
		Transaction transaction = null;

		/*
		 * On appelle la méthode générique de la classe mère pour récupérer une session
		 */
		try (Session session = openSession()) {
			transaction = session.beginTransaction();

			/*
			 * Appel hibernate pour persister (on force le type de l'entité de Serializable
			 * à Personne)
			 */
			idAffecteParHibernate = (Long) session.save(entite);

			/*
			 * Fin de la persistance donc on valide définitivement l'opération save(...)
			 * avec le commit()
			 */
			transaction.commit();

			/*
			 * On ferme la session (on nettoie les ressources utilisées) On le fait ici et
			 * pas dans le finally car on a utilisé le mécanisme Java : try-with-ressources
			 * qui gère automatiquement la fermeture de la session en cas d'erreur.
			 */
			closeSession(session);
			
		} catch (HibernateException e) {
			if (transaction != null && transaction.isActive()) {
				transaction.rollback();
			}
			LOGGER.severe("Une erreur est survenu lors de la persistance : " + e.getMessage());
		}
		return Optional.ofNullable(idAffecteParHibernate);
	}

	@Override
	public Optional<Personne> fusionner(Personne entite) {
		Personne nouvelleEntite = null;
		Transaction transaction = null;
		try (Session session = openSession()) {
			transaction = session.beginTransaction();
			nouvelleEntite = (Personne) session.merge(entite);
			transaction.commit();
			closeSession(session);
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			LOGGER.severe("Une erreur est survenu lors de la mise à jour (merge) : " + e.getMessage());
		}
		return Optional.ofNullable(nouvelleEntite);
	}

	@Override
	public void supprimer(Personne entite) {
		Transaction transaction = null;
		try (Session session = openSession()) {
			transaction = session.beginTransaction();
			session.remove(entite);
			transaction.commit();
			closeSession(session);
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			LOGGER.severe("Une erreur est survenu lors de la suppression : " + e.getMessage());
		}
	}

	@Override
	public Optional<Personne> rechercheParId(Long id) {
		Personne entite = null;
		try (Session session = openSession()) {
			entite = (Personne) session.find(Personne.class, id);
			closeSession(session);
		} catch (HibernateException e) {
			LOGGER.severe("Une erreur est survenu lors de la mise à jour (merge) : " + e.getMessage());
		}
		return Optional.ofNullable(entite);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Personne> rechercherToutesLesEntites() {
		List<Personne> listePersonnes = null;
		try (Session session = openSession()) {
			listePersonnes = session.createQuery("from Personne").getResultList();
			closeSession(session);
		} catch (HibernateException e) {
			LOGGER.severe("Une erreur est survenu lors de la recherche multiple : " + e.getMessage());
		}
		return listePersonnes;
	}

	@Override
	public Logger getLogger() {
		return LOGGER;
	}
}
