package com.tudev.isika.freeprojects.gestionpersonne.model.enums;

import java.util.Arrays;

/**
 * Enumération pour gérer les choix dans le ménu général
 */
public enum MenuChoices {

	// @formatter:off
	
	AJOUT(1), 
	MODIFICATION(2), 
	SUPPRESSION(3), 
	RECHERCHE(4), 
	LISTE(5), 
	QUITTER(0)
	;
	
	// @formatter:on

	private String choiceStringValue;

	/**
	 * Constructeur privé de l'énumération.
	 * 
	 * @param choiceNumericValue
	 */
	private MenuChoices(final int choiceNumericValue) {
		this.choiceStringValue = String.valueOf(choiceNumericValue);
	}

	/**
	 * Renvoie la chaîne de caractères représentant le choix du menu.
	 * @return
	 */
	public String getChoiceStringValue() {
		return choiceStringValue;
	}
	
	/**
	 * Vérifie si le choix de valeur indiqué en entrée est équivalent à une des
	 * valeurs de l'enum.
	 * 
	 * @param choice
	 * @return
	 */
	public static boolean contains(final String choice) {
		//@formatter:off
		
		return Arrays.asList(values())
				.stream()
				.filter(enumVal -> enumVal.choiceStringValue.equals(choice))
				.findAny()
				.isPresent();
		
		//@formatter:on

//		for (MenuChoices enumVal : values()) {
//			if (String.valueOf(enumVal.choiceNumericValue).equals(choice)) {
//				return true;
//			}
//		}
//		return false;
	}
}
