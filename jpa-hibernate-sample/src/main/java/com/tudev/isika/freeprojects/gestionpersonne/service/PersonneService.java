package com.tudev.isika.freeprojects.gestionpersonne.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import com.tudev.isika.freeprojects.gestionpersonne.main.MenuGenerator;
import com.tudev.isika.freeprojects.gestionpersonne.main.model.Personne;
import com.tudev.isika.freeprojects.gestionpersonne.main.repository.impl.PersonneRepositoryImpl;
import com.tudev.isika.freeprojects.gestionpersonne.main.repository.interfaces.IRepository;

/**
 * Service métier qui gère les opérations concernant {@link Personne}.
 */
public class PersonneService {

	private static final String NON = "non";
	private static final String OUI = "oui";

	private IRepository<Personne> personnesRepo;
	private MenuGenerator menuGenerator;

	/**
	 * Crée une nouvelle instance du service métier.
	 */
	public PersonneService() {
		this.personnesRepo = new PersonneRepositoryImpl();
		this.menuGenerator = new MenuGenerator();
	}

	/**
	 * Liste les personnes présentes en base (avec affichage de choix et de messages)
	 */
	public void listerToutesLesPersonnes() {
		List<Personne> toutesLesEntites = personnesRepo.rechercherToutesLesEntites();
		if (toutesLesEntites.isEmpty()) {
			
			menuGenerator.showEmptyLine();
			menuGenerator.showMessage("Désolé je n'ai rien trouvé en base, que voulez-vous faire ?");
			menuGenerator.showEmptyLine();
			
			// Menu général
			menuGenerator.showMainMenuSansEntete();
		} else {
			
			menuGenerator.showEmptyLine();
			
			menuGenerator.showStandardLineSeparator();
			menuGenerator.showMessage("Voici la liste des personnes présentes en base : ");
			menuGenerator.showStandardLineSeparator();
			
			toutesLesEntites.stream().forEach(personne -> menuGenerator.showMessage(personne.toString()));
			
			menuGenerator.showStandardLineSeparator();
			menuGenerator.showMessage("Que voulez-vous faire maintenant ?");
			
			menuGenerator.showEmptyLine();
			menuGenerator.showMainMenuSansEntete();
		}
	}
	
	public void showRecherchePersonneMenu(Scanner userInputScanner) {
		// recherche personne par id
		// d'ou vient l'id ?
		System.out.println("Saisir id personne : ");
		boolean isValid = false;
		Optional<Long> nextLong = null;
		do {
			try {
				nextLong = Optional.of(userInputScanner.nextLong());
				isValid = true;
			} catch(InputMismatchException e) {
				// Il y a eu une erreur de parsing sur l'id -> on redemande la dat
			}
		} while(!isValid);
		
		// ou je vais chercher ? -> repository ?
		
		// ou est donc ma personne ?
		if(nextLong.isPresent()) {
			Optional<Personne> rechercheParId = personnesRepo.rechercheParId(nextLong.get());
			if(rechercheParId.isPresent()) {
				// j'ai trouvé la personne
				Personne personne = rechercheParId.get();
				System.out.println(personne.toString());
			} else {
				System.out.println("Erreur de recherche");
			}
		}
	}

	private Personne creerPersonne(final String nom, final String prenom, final String mail, final LocalDate dateNaissance) {
		Personne personne = new Personne();
		personne.setNom(nom);
		personne.setPrenom(prenom);
		personne.setMail(mail);
		personne.setDateNaissance(dateNaissance);
		return personne;
	}
	
	public void showAjoutPersonneMenu(Scanner scanner) {

		menuGenerator.showMessage("Ajout d'une personne");
		menuGenerator.showStarsLineSeparator();
		menuGenerator.showMessage("Saisissez le nom de la personne : ");
		String nom = scanner.next();
		
		menuGenerator.showMessage("Saisissez le prénom de la personne : ");
		String prenom = scanner.next();
		
		menuGenerator.showMessage("Saisissez l'email de la personne : ");
		String mail = scanner.next();
		
		// @formatter:off
		LocalDate dateNaissance = showSaisieDateNaissanceMenu(scanner);
		
		persisterPersonne(scanner, creerPersonne(nom, prenom, mail, dateNaissance));
		
		// @formatter:on
	}

	public void persisterPersonne(Scanner scanner, Personne personne) {
		Optional<Long> optionalIdPersonne = personnesRepo.persister(personne);
		if(optionalIdPersonne.isPresent()) {
			
			menuGenerator.showMessage(String.format("Personne ajoutée avec succès (Identifiant = %d)", optionalIdPersonne.get()));
			menuGenerator.showMessage("Voulez vous ajouter une nouvelle personne ? oui (ajouter) / non (revenir au menu général)");
			
			String choix = scanner.next();
			do {
				if(OUI.equals(choix)) {
					showAjoutPersonneMenu(scanner);
				} else if(NON.equals(choix)) {
					menuGenerator.showEmptyLine();
					menuGenerator.showMainMenu();
				} else {
					menuGenerator.showMessage("Désolé, je n'ai pas compris votre choix, "
							+ "voulez vous ajouter une nouvelle personne ? "
							+ "oui (ajouter) / non (revenir au menu général)");
				}
			} while( !OUI.equals(choix) && !NON.equals(choix));
		} else {
			menuGenerator.showMessage("Erreur d'ajout de personne, voir la console pour les erreurs");
		}
	}

	private LocalDate showSaisieDateNaissanceMenu(Scanner scanner) {
		LocalDate dateNaissance = null;
		boolean isValidDate = false;
		
		do {
			menuGenerator.showMessage("Saisissez la date de naissance de la personne (format jj/mm/aaaa): ");
			String date = scanner.next();
			try {
				dateNaissance = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				// Si on arrive ici c'est que la date saisie est valide (pas d'exception de parsing)
				isValidDate = true;
			} catch(DateTimeParseException e) {
				// Il y a eu une erreur de parsing -> on redemande la dat
				menuGenerator.showMessage("La date saisie n'est pas valide (format jj/mm/aaaa), veuillez saisir une nouvelle date : ");
			}
		} while(!isValidDate);
		
		return dateNaissance;
	}

	
	
	
}
