package com.tudev.isika.freeprojects.gestionpersonne.main;

import com.tudev.isika.freeprojects.gestionpersonne.model.enums.MenuChoices;

/**
 * Générateur de menu (affiche des messages, des lignes vides, des lignes de
 * séparation)
 */
public class MenuGenerator {

	private static final String starsLineSeparator = "*********************************";
	private static final String standardLineSeparator = "-------------------------------------------------------------";

	/**
	 * Affiche une ligne vide
	 */
	public void showEmptyLine() {
		System.out.println();
	}

	/**
	 * Affiche le menu général de l'application
	 */
	public void showMainMenu() {
		showSeparator(starsLineSeparator);
		showMessage("Menu général");
		showSeparator(starsLineSeparator);

		showEmptyLine();

		showMessage(String.format("%s - Ajouter une personne", MenuChoices.AJOUT.getChoiceStringValue()));
		showMessage(String.format("%s - Modifier les informations d'une personne", MenuChoices.MODIFICATION.getChoiceStringValue()));
		showMessage(String.format("%s - Supprimer une personne", MenuChoices.SUPPRESSION.getChoiceStringValue()));
		showMessage(String.format("%s - Rechercher une personne par Identifiant", MenuChoices.RECHERCHE.getChoiceStringValue()));
		showMessage(String.format("%s - Lister toutes les personnes", MenuChoices.LISTE.getChoiceStringValue()));
		showMessage(String.format("%s - Quitter", MenuChoices.QUITTER.getChoiceStringValue()));
		
		showEmptyLine();
		showSeparator(starsLineSeparator);
	}
	
	/**
	 * Affiche le menu général de l'application sans l'entête "Menu général"
	 */
	public void showMainMenuSansEntete() {
		showEmptyLine();
		
		showMessage(String.format("%s - Ajouter une personne", MenuChoices.AJOUT.getChoiceStringValue()));
		showMessage(String.format("%s - Modifier les informations d'une personne", MenuChoices.MODIFICATION.getChoiceStringValue()));
		showMessage(String.format("%s - Supprimer une personne", MenuChoices.SUPPRESSION.getChoiceStringValue()));
		showMessage(String.format("%s - Rechercher une personne par Identifiant", MenuChoices.RECHERCHE.getChoiceStringValue()));
		showMessage(String.format("%s - Lister toutes les personnes", MenuChoices.LISTE.getChoiceStringValue()));
		showMessage(String.format("%s - Quitter", MenuChoices.QUITTER.getChoiceStringValue()));
		
		showEmptyLine();
		showSeparator(starsLineSeparator);
	}

	/**
	 * Affiche le message indiqué en entrée
	 * 
	 * @param message
	 */
	public void showMessage(final String message) {
		System.out.println(message);
	}

	/**
	 * Affiche une ligne de séparation.
	 * 
	 * @param separator
	 */
	private void showSeparator(final String separator) {
		System.out.println(separator);
	}
	
	/**
	 * 
	 */
	public void showStarsLineSeparator() {
		showSeparator(starsLineSeparator);
	}
	
	/**
	 * 
	 */
	public void showStandardLineSeparator() {
		showSeparator(standardLineSeparator);
	}
}
