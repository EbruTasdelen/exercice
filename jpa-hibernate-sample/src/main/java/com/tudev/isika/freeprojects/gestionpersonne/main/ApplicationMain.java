package com.tudev.isika.freeprojects.gestionpersonne.main;

import java.util.Scanner;

import com.tudev.isika.freeprojects.gestionpersonne.main.utils.HibernateUtil;
import com.tudev.isika.freeprojects.gestionpersonne.model.enums.MenuChoices;
import com.tudev.isika.freeprojects.gestionpersonne.service.PersonneService;

/**
 * Application principale.
 */
public class ApplicationMain {

	/**
	 * Instance statique du service métier
	 */
	private static final PersonneService service = new PersonneService();
	
	/**
	 * Instance statique du générateur de messages
	 */
	private static final MenuGenerator menuGenerator = new MenuGenerator();
	
	/**
	 * Main method.
	 * @param args
	 */
	public static void main(String[] args) {

		Scanner userInputScanner = new Scanner(System.in);
		menuGenerator.showMainMenu();
		
		while(userInputScanner.hasNext()) {
			
			String choix = userInputScanner.next();
			while( !MenuChoices.contains(choix) ) {
				menuGenerator.showMessage("Désolé, je n'ai pas compris votre choix, veuillez saisir un numéro de menu s'il vous plaît");
				menuGenerator.showMainMenu();
				choix = userInputScanner.next();
			}
			
			// Si j'arrive ici c'est que l'user a bien saisi un chiffre du menu (voir le do .. while ci-dessus)
			Integer choixNumerique = Integer.parseInt(choix);
			switch(choixNumerique) {
			case 1 :
				service.showAjoutPersonneMenu(userInputScanner);
				break;
			case 2 :
				// TODO
				// showModificationPersonneMenu();
				break;
			case 3 :
				// TODO
				// showSuppressionPersonneMenu();
				break;
			case 4 :
				// TODO
				service.showRecherchePersonneMenu(userInputScanner);
				break;
			case 5 :
				service.listerToutesLesPersonnes();
				break;
			case 0:
				menuGenerator.showMessage("Arrêt de hibernate ...");
				HibernateUtil.shutdown();
				menuGenerator.showMessage("Fermeture de l'application ...");
				System.exit(0);
			}
		}
	}
	
}

