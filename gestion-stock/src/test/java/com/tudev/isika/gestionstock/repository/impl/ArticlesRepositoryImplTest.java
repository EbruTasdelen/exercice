package com.tudev.isika.gestionstock.repository.impl;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import com.tudev.isika.gestionstock.model.Article;
import com.tudev.isika.gestionstock.model.Categorie;
import com.tudev.isika.gestionstock.repository.IRepository;

@RunWith(BlockJUnit4ClassRunner.class)
public class ArticlesRepositoryImplTest {

	private IRepository<Article> articlesRepo;

	@Before
	public void before() {
		articlesRepo = new ArticlesRepositoryImpl();
	}

	@Test
	public void shouldSetStockToOneWhenNewArticleIsAddedToRepo() {
		// Preparing data and check if repo is empty
		Article article = new Article(Long.valueOf(1), "Test", Categorie.ACCESSOIRES_MODE, new BigDecimal(10.5));
		assertEquals(0, articlesRepo.getQuantite(article));

		// Here we add the article to the repo
		articlesRepo.ajouter(article);

		// We check if the stock has been updated
		assertEquals(1, articlesRepo.getQuantite(article));
	}

	@Test
	public void shouldIncrementStockWhenExistingArticleIsAddedToRepo() {
		// Preparing data and check if repo is empty
		Article article = new Article(Long.valueOf(2), "Test", Categorie.ACCESSOIRES_MODE, new BigDecimal(20.5));
		// Here we add the article to the repo
		articlesRepo.ajouter(article);
		assertEquals(1, articlesRepo.getQuantite(article));

		// We add the same article -> quantite should be incremented
		articlesRepo.ajouter(article);

		// We check if the stock has been updated
		assertEquals(2, articlesRepo.getQuantite(article));
	}

	@Ignore
	@Test
	public void shouldDecrementStockWhenExistingArticleIsRemovedFromRepo() {
		// Preparing data and check if repo is empty
		Article article = new Article(Long.valueOf(3), "Test", Categorie.ACCESSOIRES_MODE, new BigDecimal(1.5));
		// Here we add the article to the repo
		articlesRepo.ajouter(article);
		assertEquals(1, articlesRepo.getQuantite(article));

		// We call the function to remove an article
		articlesRepo.supprimer(article);

		// We check if the stock has been updated
		assertEquals(0, articlesRepo.getQuantite(article));
	}

}
