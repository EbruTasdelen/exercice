package com.tudev.isika.gestionstock.main;

public class Application {

	public static void main(String[] args) {
		StockService stockService = new StockService();
		stockService.startService();
	}

}
