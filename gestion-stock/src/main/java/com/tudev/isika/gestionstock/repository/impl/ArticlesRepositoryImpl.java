package com.tudev.isika.gestionstock.repository.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

import com.tudev.isika.gestionstock.model.Article;
import com.tudev.isika.gestionstock.repository.IRepository;

public class ArticlesRepositoryImpl implements IRepository<Article> {

	private static final Logger LOGGER = Logger.getLogger(ArticlesRepositoryImpl.class.getSimpleName());
	private Map<Article, Integer> stockArticles = new HashMap<>();

	@Override
	public void ajouter(Article value) {
		if (stockArticles.containsKey(value)) {
			Integer oldQte = stockArticles.get(value);
			Integer newQte = oldQte + 1;
			stockArticles.put(value, newQte);
		} else {
			stockArticles.put(value, 1);
			LOGGER.info(String.format("New article is added to repo -> quantity is initialized to %d",
					stockArticles.get(value)));
		}
	}

	
	@Override
	public boolean supprimer(Article value) {
		return false;
	}

	@Override
	public Optional<Article> rechercheParId(Long id) {
		return null;
	}

	@Override
	public int getQuantite(Article value) {
		if (stockArticles.containsKey(value)) {
			return stockArticles.get(value);
		}
		return 0;
	}

}
