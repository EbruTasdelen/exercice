package com.tudev.isika.gestionstock.main;

import java.math.BigDecimal;
import java.util.logging.Logger;

import com.tudev.isika.gestionstock.model.Article;
import com.tudev.isika.gestionstock.model.Categorie;
import com.tudev.isika.gestionstock.repository.IRepository;
import com.tudev.isika.gestionstock.repository.impl.ArticlesRepositoryImpl;

public class StockService {

	private static final Logger LOGGER = Logger.getLogger(StockService.class.getSimpleName());
	
	private IRepository<Article> articlesRepository;

	public StockService() {
		LOGGER.info("StockService is created");
		articlesRepository = new ArticlesRepositoryImpl();
	}
	
	void startService() {
		// @formatter:off
		
		LOGGER.info(String.format("Starting - Available repositories are : %s", 
				articlesRepository.getClass().getSimpleName()));
		
		// Add some articles 
		Article first = new Article(1L, "Article 1", Categorie.DIVERS, new BigDecimal(10));
		Article second = new Article(2L, "Article 2", Categorie.DIVERS, new BigDecimal(20));
		Article third = new Article(3L, "Article 3", Categorie.DIVERS, new BigDecimal(30));
		Article fourth = new Article(4L, "Article 4", Categorie.DIVERS, new BigDecimal(40));
		
		// First time adding
		articlesRepository.ajouter(first);
		articlesRepository.ajouter(second);
		articlesRepository.ajouter(third);
		articlesRepository.ajouter(fourth);
		
		// Uodate quantite for first and second article
		articlesRepository.ajouter(first);
		articlesRepository.ajouter(second);
		
		// @formatter:on
	}
}
