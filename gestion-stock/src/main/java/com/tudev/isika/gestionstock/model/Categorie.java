package com.tudev.isika.gestionstock.model;

public enum Categorie {

	// @formatter:off
	
	JEUX_VIDEOS, 
	JEUX_SOCIETE, 
	RAP, 
	RAI, 
	E_LIVRES, 
	ACCESSOIRES_MODE, 
	DIVERS
	
	// @formatter:on
}
