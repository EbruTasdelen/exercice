package com.tudev.isika.gestionstock.repository;

import java.util.Optional;

/**
 * Interface représentant un dépôt générique d'objets de type T (type
 * générique).
 * 
 * @param <T> Type d'objets du repository
 */
public interface IRepository<T> {

	/**
	 * Renvoie la quantité d'objets de type T dans le dépôt.
	 * @param value
	 * @return
	 */
	int getQuantite(T value);
	
	/**
	 * Ajoute l'objet indiqué en entrée au repository.
	 * 
	 * @param value Objet à ajouter
	 */
	void ajouter(T value);

	/**
	 * Supprime l'objet du dépôt.
	 * 
	 * @param value Objet à supprimer
	 * @return <b>true</b> si l'objet à été supprimé, <b>false</b> sinon.
	 */
	boolean supprimer(T value);

	/**
	 * Renvoie l'objet dont l'identifiant est indiqué en entrée.
	 * 
	 * @param id Identifiant de l'objet à récupérer
	 * @return T s'il existe dans le dépôt, <b>null</b> sinon.
	 */
	Optional<T> rechercheParId(Long id);

}
