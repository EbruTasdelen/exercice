package com.tudev.isika.gestionstock.model;

import java.math.BigDecimal;

public class Article {

	private Long id;
	private String designation;
	private Categorie categorie;
	private BigDecimal prixUnitaire;

	public Article(Long id, String designation, Categorie categorie, BigDecimal prixUnitaire) {
		this.id = id;
		this.designation = designation;
		this.categorie = categorie;
		this.prixUnitaire = prixUnitaire;
	}

	public void changerDesignation(String designation) {
		setDesignation("ART_" + designation);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public BigDecimal getPrixUnitaire() {
		return prixUnitaire;
	}

	public void setPrixUnitaire(BigDecimal prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}

}
