package com.Isika.cdi5.table.hibernate.main.tp_banque.utilisateurs;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.Isika.cdi5.table.hibernate.main.tp_banque.banque.CompteBancaire;

@Entity
public class Client {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nom;
	private String prenom;
	private String adresse;
	private String mail;
	private String telephone;
	
	@OneToOne(mappedBy = "client",orphanRemoval = true,cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
	private CompteBancaire compteBancaire;

	public Client() {

	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Long getId() {
		return id;

	}
	public CompteBancaire getCompteBancaire() {
		return compteBancaire;
	}
	public void attacherCompteBancaire(CompteBancaire compteBancaire) {
		compteBancaire.setClient(this);
		this.compteBancaire = compteBancaire;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Client [id=");
		builder.append(id);
		builder.append(", nom=");
		builder.append(nom);
		builder.append(", prenom=");
		builder.append(prenom);
		builder.append(", adresse=");
		builder.append(adresse);
		builder.append(", mail=");
		builder.append(mail);
		builder.append(", telephone=");
		builder.append(telephone);
		builder.append("]");
		return builder.toString();
	}

}
