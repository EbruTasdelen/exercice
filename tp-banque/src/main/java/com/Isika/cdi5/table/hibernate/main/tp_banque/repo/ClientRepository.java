package com.Isika.cdi5.table.hibernate.main.tp_banque.repo;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.Isika.cdi5.table.hibernate.main.tp_banque.utilisateurs.Client;
import com.Isika.cdi5.table.hibernate.main.tp_banque.utils.HibernateUtil;

public class ClientRepository {
	private EntityManager entityManager;
	
	
	
		public ClientRepository() {
			this.entityManager= HibernateUtil.createEntityManager();
		}
		public Long InsertClient (Client client ) {
			EntityTransaction transaction = entityManager.getTransaction();
			transaction.begin();
			entityManager.persist(client);
			entityManager.flush();
			transaction.commit();
			return client.getId();
			
			
		}
	}


