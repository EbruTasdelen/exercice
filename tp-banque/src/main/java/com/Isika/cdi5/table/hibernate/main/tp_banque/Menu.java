package com.Isika.cdi5.table.hibernate.main.tp_banque;



public class Menu {
	
	private static final String Separator = "........................................";
	private void showEmptyLine() {
		System.out.println();
	}
	
	public void showMainMenu() {
		showSeparator(Separator);
		showMessage("Banque ABC - Bienvenue");
		showEmptyLine();
		showMessage("Choisissez une opération : ");
		showEmptyLine();
		showMessage(String.format("%s - Ajouter un utilisateur", MenuOption.AJOUT.getChoiceStringValue()));
		showMessage(String.format("%s - Modifier les informations d'un utilisateur", MenuOption.MODIFICATION.getChoiceStringValue()));
		showMessage(String.format("%s - Supprimer un utilisateur", MenuOption.SUPPRESSION.getChoiceStringValue()));
		showMessage(String.format("%s - Effectuer un retrait ", MenuOption.VIREMENT.getChoiceStringValue()));
		showMessage(String.format("%s - Faire un virement ", MenuOption.RETRAIT.getChoiceStringValue()));
		showMessage(String.format("%s - Quitter", MenuOption.QUITTER.getChoiceStringValue()));
		
		showEmptyLine();
		showSeparator(Separator);
	}
	

	
	public void showMessage(final String message) {
		System.out.println(message);
	}

	
	private void showSeparator(final String separator) {
		System.out.println(separator);
	}
	
	
	public void showStarsLineSeparator() {
		showSeparator(Separator);
	}
	

}



