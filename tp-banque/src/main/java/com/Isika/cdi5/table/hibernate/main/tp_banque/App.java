package com.Isika.cdi5.table.hibernate.main.tp_banque;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.dom4j.tree.ElementNameIterator;

import com.Isika.cdi5.table.hibernate.main.tp_banque.banque.CompteBancaire;
import com.Isika.cdi5.table.hibernate.main.tp_banque.banque.TypeCompteBancaire;
import com.Isika.cdi5.table.hibernate.main.tp_banque.repo.ClientRepository;

import com.Isika.cdi5.table.hibernate.main.tp_banque.service.ComptesService;
import com.Isika.cdi5.table.hibernate.main.tp_banque.utilisateurs.Client;
import com.Isika.cdi5.table.hibernate.main.tp_banque.utils.HibernateUtil;

public class App {

	private static final Menu menu = new Menu();
	private static final ComptesService service = new ComptesService();

	public static void main(String[] args) {

		ClientRepository repository = new ClientRepository();
		Client client = new Client();
		client.setNom("Martin");
		client.setPrenom("Paul");
		client.setAdresse("14 av de la Republique");
		client.setMail("paulm@gmail.com");
		client.setTelephone("0212451425");

		CompteBancaire compteBancaire = new CompteBancaire();
		compteBancaire.setNumeroCompte(UUID.randomUUID().toString());
		compteBancaire.setDateValidite(new Date());
		compteBancaire.setType(TypeCompteBancaire.COMPTECOURANT);
		compteBancaire.setSolde(new BigDecimal(1258.20));

		client.attacherCompteBancaire(compteBancaire);
		repository.InsertClient(client);

		EntityManager entityManager = HibernateUtil.createEntityManager();
		CompteBancaire compteBancaire2 = entityManager.find(CompteBancaire.class, client.getCompteBancaire().getId());
		compteBancaire2.depot(new BigDecimal(50));

	
		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();
		entityManager.merge(compteBancaire2);// update
		entityManager.flush();
		transaction.commit();
		System.out.println(compteBancaire2);

		System.out.println(client);

		HibernateUtil.closeAll();

		/*
		 * Scanner userInputScanner = new Scanner(System.in); menu.showMainMenu();
		 * 
		 * while(userInputScanner.hasNext()) {
		 * 
		 * String choix = userInputScanner.next(); while( !MenuOption.contains(choix) )
		 * { menu.showMessage(" veuillez saisir un numéro  ");
		 * menu.showMainMenu(); choix = userInputScanner.next(); }
		 * 
		 * 
		 * Integer choixNumerique = Integer.parseInt(choix); switch(choixNumerique) {
		 * case 1 : service.creerCompte(); break; case 2 :
		 * 
		 *  break; case 3 :
		 * 
		 * // break; case 4 :
		 * 
		 * // break; case 5 :
		 * // break; case 0:
		 * menu.showMessage("Arrêt de Hibernate "); //HibernateUtil.shutdown();
		 * menu.showMessage("Fermeture de l'application"); System.exit(0);
		 */
	}

	// }
}
//}
