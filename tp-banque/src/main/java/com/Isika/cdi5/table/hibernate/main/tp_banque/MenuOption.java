package com.Isika.cdi5.table.hibernate.main.tp_banque;

import java.util.Arrays;

public enum MenuOption {
	
	AJOUT(1), MODIFICATION(2), SUPPRESSION(3), VIREMENT(4), RETRAIT(5), QUITTER(0);


private String choiceStringValue;


private MenuOption(final int choiceNumericValue) {
	this.choiceStringValue = String.valueOf(choiceNumericValue);
}

public String getChoiceStringValue() {
	return choiceStringValue;
}

public static boolean contains(final String choice) {
	return Arrays.asList(values())
			.stream()
			.filter(enumVal -> enumVal.choiceStringValue.equals(choice))
			.findAny()
			.isPresent();
	
	

}
}
