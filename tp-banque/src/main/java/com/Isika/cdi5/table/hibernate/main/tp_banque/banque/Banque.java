package com.Isika.cdi5.table.hibernate.main.tp_banque.banque;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;





@Entity
public class Banque {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long identifiant;
	private String nomBanque;
	private  String adresseBanque;
	private String telephoneBanque;
	private String mailBanque;
	
	@OneToMany(mappedBy = "banque", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<CompteBancaire> comptes;

	public Banque() {
		this.comptes = new ArrayList<>();
	}
	public Long getIdentifiant() {
		return identifiant;
	}

	public void setIdentifiant(Long identifiant) {
		this.identifiant = identifiant;
	}

	public String getNomBanque() {
		return nomBanque;
	}

	public void setNomBanque(String nomBanque) {
		this.nomBanque = nomBanque;
	}

	public String getAdresseBanque() {
		return adresseBanque;
	}

	public void setAdresseBanque(String adresseBanque) {
		this.adresseBanque = adresseBanque;
	}

	public String getTelephoneBanque() {
		return telephoneBanque;
	}

	public void setTelephoneBanque(String telephoneBanque) {
		this.telephoneBanque = telephoneBanque;
	}

	public String getMailBanque() {
		return mailBanque;
	}

	public void setMailBanque(String mailBanque) {
		this.mailBanque = mailBanque;
	}

	public List<CompteBancaire> getComptes() {
		return comptes;
	}

	public void setComptes(List<CompteBancaire> comptes) {
		this.comptes = comptes;
	}
	
	public void addModule(CompteBancaire compte) {
		//compte.setBanque(this);
		this.comptes.add(compte);
	}
	
	public void removeModule(CompteBancaire module) {
	//module.setBanque(null);
		this.comptes.remove(module);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Banque[identifiant=");
		builder.append(identifiant);
		builder.append(", nomBanque=");
		builder.append(nomBanque);
		builder.append(", adresseBanque=");
		builder.append(adresseBanque);
		builder.append(", telephoneBanque=");
		builder.append(telephoneBanque);
		builder.append(", mailBanque=");
		builder.append(mailBanque);
		builder.append("]");
		return builder.toString();
	}




	
	

}
