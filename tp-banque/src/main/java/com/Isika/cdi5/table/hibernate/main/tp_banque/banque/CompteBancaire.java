package com.Isika.cdi5.table.hibernate.main.tp_banque.banque;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.Isika.cdi5.table.hibernate.main.tp_banque.utilisateurs.Client;

@Entity
public class CompteBancaire {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String numeroCompte;
	
	@Temporal(TemporalType.DATE)
	private Date dateValidite;
	
	private BigDecimal solde;
	
	@Enumerated(EnumType.STRING)
	private TypeCompteBancaire type;
	
	@ManyToOne
	private Banque banque;
	
	@OneToOne()
	private Client client;

	
	public CompteBancaire() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumeroCompte() {
		return numeroCompte;
	}

	public void setNumeroCompte(String numeroCompte) {
		this.numeroCompte = numeroCompte;
	}

	public Date getDateValidite() {
		return dateValidite;
	}

	public void setDateValidite(Date dateValidite) {
		this.dateValidite = dateValidite;
	}

	public BigDecimal getSolde() {
		return solde;
	}

	public void setSolde(BigDecimal solde) {
		this.solde = solde;
	}

	public TypeCompteBancaire getType() {
		return type;
	}

	public void setType(TypeCompteBancaire type) {
		this.type = type;
	}
public void depot(BigDecimal valeur ) {
	System.out.println("L'ancien solde est de " + this.solde);
	this.solde= new BigDecimal(solde.doubleValue()).add(valeur);//?
	System.out.println("Le nouveau solde est de " + this.solde);
	
}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CompteBancaire [id=");
		builder.append(id);
		builder.append(", numeroCompte=");
		builder.append(numeroCompte);
		builder.append(", dateValidite=");
		builder.append(dateValidite);
		builder.append(", solde=");
		builder.append(solde);
		builder.append(", type=");
		builder.append(type);
		builder.append(", banque=");
		builder.append(banque);
		builder.append("]");
		return builder.toString();
	}

	public void setClient(Client client) {
		this.client = client;
		
		
	}

		
	}
	
	
	


