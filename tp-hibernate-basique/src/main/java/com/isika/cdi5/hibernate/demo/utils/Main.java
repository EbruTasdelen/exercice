package com.isika.cdi5.hibernate.demo.utils;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

import com.isika.cdi5.hibernate.demo.model.Article;
import com.isika.cdi5.hibernate.demo.model.ArticleRepository;
import com.isika.cdi5.hibernate.demo.model.Categorie;

public class Main {

	public static void main(String[] args) {

		/* 
		 * Etapes du programme
		 * 
		 * 1 - Je crée une nouvelle instance de Article (en remplissant tous les champs par les setters)
		 * 
		 * 2 - Je crée une instance de ArticleRepository pour appeler la méthode persister(...)
		 * 
		 * 3 - J'utilise la même instance du repository pour récupérer l'article que j'ai sauvegardé 
		 * avec son id (que je dois stocker ou afficher dans la console)
		 * 
		 * 4 - Je modifie une propriété de mon article et j'essaie de le mettre à jour avec la méthode
		 * modifier du repository
		 * 
		 * 5 - pour finir je supprime l'article en utilisant l'identifiant déjà stocké
		 */
		Article jeuSteam = new Article();
		jeuSteam.setIntitule("The Long Dark");
		jeuSteam.setDateValidite(Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		jeuSteam.setDescription("Description : Jeu de survie dans le nord glacial du Canada");
		jeuSteam.setQuantite(1);
		jeuSteam.setDisponibe(true);
		jeuSteam.setCategorie(Categorie.JEUX);
		
		ArticleRepository repository = new ArticleRepository();
		Long id = repository.persisterAvecSave(jeuSteam);
		
		Optional<Article> article = repository.rechercheParId(id);
		if(article.isPresent()) {
			System.out.println("Article trouvé : " + article.get());
		} else {
			System.out.println("Aucun article correspondant à l'id : " + id);
			repository.supprimerArticle(jeuSteam); 
		}
		
		// Je dois laisser cette ligne pour que Hibernate termine 
		// la session proprement et nettoyer les ressources 
		HibernateUtil.shutdown();
	}

}
