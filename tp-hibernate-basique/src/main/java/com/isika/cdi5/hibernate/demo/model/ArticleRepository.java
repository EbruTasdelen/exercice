package com.isika.cdi5.hibernate.demo.model;

import java.util.Optional;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.isika.cdi5.hibernate.demo.utils.HibernateUtil;

public class ArticleRepository {

	public Long persister(Article article) {
		/*
		 * Etapes pour l'utilisation des sessions hibernate
		 * 
		 * 1 - Appel à la méthode getSession()
		 * 
		 * 2 - Récupérer une session hibernate
		 * 
		 * 3 - j'appelle la méthode persist(...) avec l'article que j'ai reçu en paramètre
		 * 
		 * 4 - vu que le méthode persist ne renvoie RIEN
		 * (Il faut utiliser la méthode save(....) qui elle renvoie un objet serializable (en général c'est l'id généré par hibernate)
		 * je l'affiche pour l'utiliser pour la suppresion ou la recherche plus tard
		 * 
		 * 5 - si j'obtiens un id c'est que l'article a été persisté en base et tout va bien 
		 */
		Session session = getSession();
		Transaction transaction = session.beginTransaction();
		session.persist(article);

		// L'appel à la méthode flush permet de forcer l'attribution de l'id à l'objet article
		session.flush();
		transaction.commit();
		
		// On n'oublie pas de fermer la session hibernate 
		closeSession(session);
		
		System.out.println("Article persisté avec id : " + article.getIdentifiant());
		return article.getIdentifiant();
	}
	
	public Long persisterAvecSave(Article article) {
		Session session = getSession();
		Transaction transaction = session.beginTransaction();
		Long id = (Long) session.save(article);
		transaction.commit();
		session.close();
		System.out.println("Article persisté avec id : " + id);
		return id;
	}
	
	public Optional<Article> rechercheParId(Long identifiantArticle) {
		Session session = getSession();
		Article article = session.find(Article.class, identifiantArticle);
		Optional<Article> nullableArticle = Optional.ofNullable(article);
		session.close();
		return nullableArticle;
	}
	
	public boolean supprimerArticle(Article jeuSteam) {
		Session session = getSession();
		Transaction transaction = session.beginTransaction();
		session.delete(jeuSteam);
		transaction.commit();
		closeSession(session);
		return true;
		
		
	}

//	public boolean modifierArticle(Article article) {
//		Session session = getSession();
//		 Transaction transaction = session.beginTransaction();
//		 boolean isUpdate = false;
//		session.close();
//		  merge
//		 return isUpdate;
//}
	
	
	
	private Session getSession() {
		return HibernateUtil.getSessionFactory().openSession();
	}
	
	private void closeSession(Session session) {
		session.close();
	}
}
