package com.isika.cdi5.hibernate.demo.model;

public enum Categorie {

	// @formatter:off
	
	JEUX, 
	LIVRES, 
	MUSIQUE

	// @formatter:on
}
