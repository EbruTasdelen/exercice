package com.isika.cdi5.jdbc.adresses;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.isika.cdi5.jdbc.main.DefaultH2DatabaseConnection;

public class GestionAdresses {

	private DefaultH2DatabaseConnection dbConnection;
	
	public GestionAdresses(DefaultH2DatabaseConnection dbConnection) {
		this.dbConnection = dbConnection;
	}
	
	public void creerTableAdresses() {
		System.out.println("**** Création de la table : Adresses ****");
		try (Connection connection = dbConnection.getConnection()) {
			Statement statement = connection.createStatement();
			int suppression = statement.executeUpdate("DROP TABLE Adresses IF EXISTS");
			if(suppression != -1) {
				int result = statement.executeUpdate("CREATE TABLE Adresses (ID INTEGER PRIMARY KEY, ADRESSE VARCHAR(255),COMPLEMENTADRESSE VARCHAR(255),CODEPOSTAL INTEGER,VILLE VARCHAR(255))");
				if(result != -1) {
					System.out.println("Table Adresses créée");
				}
			}
			// Fermeture des ressources
			statement.close();
			connection.close();
		} catch (SQLException e) {
			System.err.println(e);
		}
		System.out.println("**** Fin de création de la table : Adresses ****");
	}
	
	public void afficherToutesLesAdresses() throws SQLException {
		Adresse adresse = null;
		try (Connection connection = dbConnection.getConnection()) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM Adresses");
			while (resultSet.next()) {
				adresse = new Adresse(resultSet.getInt("ID"), 
						resultSet.getString("ADRESSE"),
						resultSet.getString("COMPLEMENTADRESSE"),
						resultSet.getInt("CODEPOSTAL"),
						resultSet.getString("VILLE"));
				System.out.println(adresse);
			}
			// Fermeture des ressources
			resultSet.close();
			statement.close();
			connection.close();
		} catch (SQLException e) {
			System.err.println(e);
		}
	}

	public boolean ajouterAdresse(Adresse adresse) {
		boolean ajoutAdresse = false;
		try (Connection connection = dbConnection.getConnection()) {
			
			PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Adresses VALUES (?,?,?,?,?)");
			preparedStatement.setInt(1, adresse.getId());
			preparedStatement.setString(2, adresse.getAdresse());
			preparedStatement.setString(3, adresse.getComplementAdresse());
			preparedStatement.setInt(4, adresse.getCodePostal());
			preparedStatement.setString(5, adresse.getVille());
			
			int result = preparedStatement.executeUpdate();
			if(result > 0) {
				ajoutAdresse = true;
			}
			
			// Fermeture des ressources
			preparedStatement.close();
			connection.close();
		} catch (SQLException e) {
			System.err.println(e);
		}
		return ajoutAdresse;
	}
	
}
