package com.isika.cdi5.jdbc.main;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.isika.cdi5.jdbc.adresses.Adresse;
import com.isika.cdi5.jdbc.articles.Article;
import com.isika.cdi5.jdbc.personnes.Personne;

public class ConstantData {

	// @formatter:off

	public static final List<Integer> LISTE_IDENTIFIANTS = IntStream.range(0, 5).boxed().collect(Collectors.toList());
	
	public static final Article[] TAB_ARTICLES = { 
			new Article(1, "ArticleNumero_1", new BigDecimal(10.5).setScale(2, RoundingMode.DOWN)),
			new Article(2, "ArticleNumero_2", new BigDecimal(22.25).setScale(2, RoundingMode.DOWN)),
			new Article(3, "ArticleNumero_3", new BigDecimal(25.1).setScale(2, RoundingMode.DOWN)),
			new Article(4, "ArticleNumero_4", new BigDecimal(10).setScale(2, RoundingMode.DOWN)),
			new Article(5, "ArticleNumero_5", new BigDecimal(18.35).setScale(2, RoundingMode.DOWN)) 
	};

	public static final Adresse[] TAB_ADRESSES = { new Adresse(1, "23 rue de la Victoire", "1", 24004, "LaPlage"),
			new Adresse(2, "17 rue du petit Buisson", "2", 94012, "LaBas"),
			new Adresse(3, "42 rue de la Pompe", "3", 44002, "Nantes"),
			new Adresse(4, "12 rue du Sapin", "4", 75014, "Paris"),
			new Adresse(5, "14 rue du bois", "5", 41032, "Fin Fond de la France") 
	};

	public static final Personne[] TAB_PERSONNES = {
			new Personne(1, "Fauvel", "Alexandre", LocalDate.of(2001, 3, 20), TAB_ADRESSES[0]),
			new Personne(2, "Martin", "Ebru", LocalDate.of(1996, 2, 5), TAB_ADRESSES[1]),
			new Personne(3, "Paul", "Juliette", LocalDate.of(2010, 3, 20), TAB_ADRESSES[2]),
			new Personne(4, "Jean", "Anthony", LocalDate.of(1996, 3, 20), TAB_ADRESSES[3]),
			new Personne(5, "Robert", "Thomas", LocalDate.of(1990, 3, 20), TAB_ADRESSES[4]) 
	};

	// @formatter:on
}
