package com.isika.cdi5.jdbc.articles;

import java.math.BigDecimal;

public class Article {

	private Integer id;
	private String designation;
	private BigDecimal prix;

	public Article(Integer id, String designation, BigDecimal prix) {
		this.id = id;
		this.designation = designation;
		this.prix = prix;
	}

	public Integer getId() {
		return id;
	}

	public String getDesignation() {
		return designation;
	}

	public BigDecimal getPrix() {
		return prix;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Article [id=");
		builder.append(id);
		builder.append(", designation=");
		builder.append(designation);
		builder.append(", prix=");
		builder.append(prix);
		builder.append("]");
		return builder.toString();
	}

}
