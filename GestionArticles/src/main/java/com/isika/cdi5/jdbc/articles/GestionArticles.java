package com.isika.cdi5.jdbc.articles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.isika.cdi5.jdbc.main.DefaultH2DatabaseConnection;


public class GestionArticles {

	private DefaultH2DatabaseConnection dbConnection;
	
	public GestionArticles(DefaultH2DatabaseConnection dbConnection) {
		this.dbConnection = dbConnection;
	}
	
	public void creerTableArticles() {
		System.out.println("**** Création de la table : Articles ****");

		try (Connection connection = dbConnection.getConnection()) {
		
			Statement statement = connection.createStatement();
			int suppression = statement.executeUpdate("DROP TABLE Articles IF EXISTS");
			if(suppression != -1) {
				System.out.println("Table Articles trouvée et supprimée");
				int result = statement.executeUpdate("CREATE TABLE Articles (ID INTEGER PRIMARY KEY, DESIGNATION VARCHAR(255), PRIX DECIMAL)");
				if(result != -1) {
					System.out.println("Table Articles créée");
				}
			}
			
			// Fermeture des ressources
			statement.close();
			connection.close();
		} catch (SQLException e) {
			System.err.println(e);
		}
		System.out.println("**** Fin de création de la table : Articles ****");
	}
	
	public void afficherTousLesArticles() throws SQLException {
		Article article = null;
		try (Connection connection = dbConnection.getConnection()) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM Articles");
			while (resultSet.next()) {
				article = new Article(resultSet.getInt("ID"),
						resultSet.getString("DESIGNATION"),
						resultSet.getBigDecimal("PRIX"));
				System.out.println(article);
			}
			
			// Fermeture des ressources
			resultSet.close();
			statement.close();
			connection.close();
		} catch (SQLException e) {
			System.err.println(e);
		}
	}

	public boolean ajouterArticle(Article article) {
		boolean articleInsere = false;
		try (Connection connection = dbConnection.getConnection()) {
			
			PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Articles VALUES (?,?,?)");
			preparedStatement.setInt(1, article.getId());
			preparedStatement.setString(2, article.getDesignation());
			preparedStatement.setBigDecimal(3, article.getPrix());
			
			int result = preparedStatement.executeUpdate();
			if(result > 0) {
				articleInsere = true;
			}
			preparedStatement.close();
			connection.close();
		} catch (SQLException e) {
			System.err.println(e);
		}
		return articleInsere;
	}
	
}
