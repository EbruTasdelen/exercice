package com.isika.cdi5.jdbc.main;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DefaultH2DatabaseConnection {

	private static final String DRIVER_NAME = "org.h2.Driver";
	private String connectionString;
	private String userName;
	private String password;

	public DefaultH2DatabaseConnection(String connectionString) {
		this.connectionString = connectionString;
		this.userName = "sa";
		this.password = "";
		init();
	}

	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection(connectionString, userName, password);
	}

	private void init() {
		try {
			Class<?> driverClass = Class.forName(DRIVER_NAME);
			Driver jdbcDriver = (Driver) driverClass.newInstance();
			DriverManager.registerDriver(jdbcDriver);
		} catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException e) {
			System.err.println(e);
		}
	}
}
