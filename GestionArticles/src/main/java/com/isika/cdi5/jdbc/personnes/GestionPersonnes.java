package com.isika.cdi5.jdbc.personnes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.isika.cdi5.jdbc.adresses.Adresse;
import com.isika.cdi5.jdbc.main.DefaultH2DatabaseConnection;

public class GestionPersonnes {

	private DefaultH2DatabaseConnection dbConnection;
	
	public GestionPersonnes(DefaultH2DatabaseConnection dbConnection) {
		this.dbConnection = dbConnection;
	}
	
	public void creerTablePersonnes() {
		System.out.println("**** Création de la table : Personnes ****");
		try (Connection connection = dbConnection.getConnection()) {
			Statement statement = connection.createStatement();
			int suppression = statement.executeUpdate("DROP TABLE Personnes IF EXISTS");
			if(suppression != -1) {
				System.out.println("Table Personnes trouvée et supprimée");
				int result = statement.executeUpdate("CREATE TABLE Personnes (ID INTEGER PRIMARY KEY, NOM VARCHAR(255), PRENOM VARCHAR(255),BIRTHDAY DATE, IDADRESSE INTEGER)");
				if(result != -1) {
					System.out.println("Table Personnes créée");
				}
			}
			// Fermeture des ressources
			statement.close();
			connection.close();
		} catch (SQLException e) {
			System.err.println(e);
		}
		System.out.println("**** Fin de création de la table : Personnes ****");
	}
	
	public void afficherToutesLesPersonnes() throws SQLException {
		try (Connection connection = dbConnection.getConnection()) {
			
			Statement statement = connection.createStatement();
			ResultSet resultSetPersonnes = statement.executeQuery("SELECT * FROM Personnes");
			
			while (resultSetPersonnes.next()) {
				
				// On reconstruit l'objet personne
				Personne personne = new Personne(
						resultSetPersonnes.getInt("ID"),
						resultSetPersonnes.getString("NOM"),
						resultSetPersonnes.getString("PRENOM"),
						resultSetPersonnes.getDate("BIRTHDAY").toLocalDate());
				
				// Il faut récupérer l'id de l'adresse pour la charger depuis la base
				int idAdresse = resultSetPersonnes.getInt("IDADRESSE");
				
				// Lancer une requête de sélection sur la table adresses avec l'identifiant récupéré
				Adresse adresse = null;
				Statement statementAdresses = connection.createStatement();
				ResultSet resultSetAdresses = statementAdresses.executeQuery("SELECT * FROM Adresses A WHERE A.ID = " + idAdresse);
				while(resultSetAdresses.next()) {
					// Une fois ici, nous sommes sûrs d'avoir récupéré l'adresse 
					// avec l'identifiant depuis la personne
					adresse = new Adresse(idAdresse, 
							resultSetAdresses.getString("ADRESSE"),
							resultSetAdresses.getString("COMPLEMENTADRESSE"),
							resultSetAdresses.getInt("CODEPOSTAL"),
							resultSetAdresses.getString("VILLE"));
					
					// Ensuite on affecte l'objet adresse à l'objet personne
					personne.setAdresse(adresse);
					
					// On termine la boucle
					break;
				}
				resultSetAdresses.close();
				statementAdresses.close();
				
				// Affichage
				System.out.println(personne);
			}
			
			// Fermeture des ressources
			resultSetPersonnes.close();
			statement.close();
			connection.close();
		} catch (SQLException e) {
			System.err.println(e);
		}
	}

	public boolean ajouterPersonne(Personne personne) {
		boolean insertionPersonneOk = false;
		try (Connection connection = dbConnection.getConnection()) {
			
			PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Personnes VALUES (?,?,?,?,?)");
			preparedStatement.setInt(1, personne.getId());
			preparedStatement.setString(2, personne.getNom());
			preparedStatement.setString(3, personne.getPrenom());
			preparedStatement.setDate(4, java.sql.Date.valueOf(personne.getBirthday()));
			
			// Il faut insérer la clé étrangère de Adresse, manuellement
			// Le lien sera aussi fait manuellement (voir méthode d'affichage) 
			preparedStatement.setInt(5, personne.getAdresse().getId());
		
			int result = preparedStatement.executeUpdate();
			if(result > 0) {
				insertionPersonneOk = true;
			}
			
			// Fermeture des ressources
			preparedStatement.close();
			connection.close();
		} catch (SQLException e) {
			System.err.println(e);
		}
		return insertionPersonneOk;
	}
	
}
