package com.isika.cdi5.jdbc.main;

import java.sql.SQLException;

import com.isika.cdi5.jdbc.adresses.GestionAdresses;
import com.isika.cdi5.jdbc.articles.GestionArticles;
import com.isika.cdi5.jdbc.personnes.GestionPersonnes;

public class Main {
	
	// Vous pouvez utiliser cette base de données "MyDatabase" 
	// mais il faut créer le répertoire DonneesDeTest d'abord
	// ensuite lancer H2 en mode embedded en saisissant MyDatabase avec une première connexion
	// ensuite il faut se déconnecter et se reconnecter en mode (server) avec le même nom de la base
	
	// Si cela ne fonctionne pas, remplacez par votre chemin de base de données et on essaiera de le faire en live
	
	// Le fait d'utiliser le mode tcp/localhost en mode serveur, c'est plus rapide et plus fiable
	// Car il permet plusieurs connexions simultanées
	private static final String DEFAULT_CONNECTION_STRING = "jdbc:h2:tcp://localhost/c:/DonneesDeTest/MyDatabase";
	
	private static DefaultH2DatabaseConnection databaseConnection = new DefaultH2DatabaseConnection(DEFAULT_CONNECTION_STRING);
	private static GestionAdresses gestionAdresses = new GestionAdresses(databaseConnection);
	private static GestionPersonnes gestionPersonnes = new GestionPersonnes(databaseConnection);
	private static GestionArticles gestionArticles = new GestionArticles(databaseConnection);
	
	public static void main(String[] args) throws SQLException {
		
		// Articles
		gestionArticles.creerTableArticles();
		System.out.println();
		System.out.println("**** Insertion de plusieurs Article ****");
		ConstantData.LISTE_IDENTIFIANTS.stream().forEach(intValue -> {
			boolean ajoute = gestionArticles.ajouterArticle(ConstantData.TAB_ARTICLES[intValue]);
			if(ajoute) {
				System.out.println("Nouvel Article inséré [Id=" + intValue + "]");
			}
		});
		System.out.println("**** Fin d'insertion de plusieurs Article ****");
		System.out.println();
		System.out.println("**** Affichage du contenu de la table : Articles ****");
		gestionArticles.afficherTousLesArticles();
		System.out.println();

		// Adresses 
		gestionAdresses.creerTableAdresses();
		System.out.println();
		System.out.println("**** Insertion de plusieurs Adresse ****");
		ConstantData.LISTE_IDENTIFIANTS.stream().forEach(intValue -> {
			boolean ajoutee = gestionAdresses.ajouterAdresse(ConstantData.TAB_ADRESSES[intValue]);
			if(ajoutee) {
				System.out.println("Nouvelle Adresse insérée [Id=" + intValue + "]");
			}
		});
		System.out.println("**** Fin d'insertion de plusieurs Adresses ****");
		
		System.out.println();
		System.out.println("**** Affichage du contenu de la table : Adresses ****");
		gestionAdresses.afficherToutesLesAdresses();
		System.out.println();
		
		// Personnes
		gestionPersonnes.creerTablePersonnes();
		System.out.println();
		System.out.println("**** Insertion de plusieurs Personne ****");
		ConstantData.LISTE_IDENTIFIANTS.stream().forEach(intValue -> {
			boolean ajoutee = gestionPersonnes.ajouterPersonne(ConstantData.TAB_PERSONNES[intValue]);
			if(ajoutee) {
				System.out.println("Nouvelle Personne insérée [Id=" + intValue + "]");
			}
		});
		System.out.println("**** Fin d'insertion de plusieurs Personne ****");
		System.out.println();
		System.out.println("**** Affichage du contenu de la table : Personnes ****");
		gestionPersonnes.afficherToutesLesPersonnes();
	}

}
