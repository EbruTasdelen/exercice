package com.isika.cdi5.jdbc.adresses;

public class Adresse {

	private Integer id;
	private String adresse;
	private String complementAdresse;
	private Integer codePostal;
	private String ville;

	public Adresse(Integer id, String adresse, String complementAdresse, Integer codePostal, String ville) {
		this.id = id;
		this.adresse = adresse;
		this.complementAdresse = complementAdresse;
		this.codePostal = codePostal;
		this.ville = ville;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getComplementAdresse() {
		return complementAdresse;
	}

	public void setComplementAdresse(String complementAdresse) {
		this.complementAdresse = complementAdresse;
	}

	public Integer getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(Integer codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Adresse [id=");
		builder.append(id);
		builder.append(", adresse=");
		builder.append(adresse);
		builder.append(", complementAdresse=");
		builder.append(complementAdresse);
		builder.append(", codePostal=");
		builder.append(codePostal);
		builder.append(", ville=");
		builder.append(ville);
		builder.append("]");
		return builder.toString();
	}
	
}
