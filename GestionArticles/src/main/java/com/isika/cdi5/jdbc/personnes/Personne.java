package com.isika.cdi5.jdbc.personnes;

import java.time.LocalDate;

import com.isika.cdi5.jdbc.adresses.Adresse;

public class Personne {

	private Integer id;
	private String nom;
	private String prenom;
	private LocalDate birthday;
	private Adresse adresse;

	public Personne(Integer id, String nom, String prenom, LocalDate birthday) {
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.birthday = birthday;
	}
	
	public Personne(Integer id, String nom, String prenom, LocalDate birthday, Adresse adresse) {
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.birthday = birthday;
		this.adresse = adresse;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Personne [id=");
		builder.append(id);
		builder.append(", nom=");
		builder.append(nom);
		builder.append(", prenom=");
		builder.append(prenom);
		builder.append(", birthday=");
		builder.append(birthday);
		builder.append(", adresse=");
		builder.append(adresse);
		builder.append("]");
		return builder.toString();
	}
	
	
}
