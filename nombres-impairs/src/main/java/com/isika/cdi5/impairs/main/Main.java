package com.isika.cdi5.impairs.main;

import java.util.List;

import com.isika.cdi5.impairs.NombreImpairs;

public class Main {

	public static void main(String[] args) {
		// Exercice 3 : Nombres impairs, d'un entier donné
		// Même consigne pour la saisie d'un nombre, on peut utiliser un scanner
		int nombre = 15;
		NombreImpairs impairs = new NombreImpairs();
		List<Integer> impairsInferieurs = impairs.genererNombresImpairsInferieurs(nombre);
		System.out.println(String.format("Il y'a %d nombres impairs inférieurs à %d :", 
				impairsInferieurs.size(), nombre));
		impairsInferieurs.stream().forEach(impair -> System.out.println(impair));
	}

}
