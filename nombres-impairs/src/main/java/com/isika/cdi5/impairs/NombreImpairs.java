package com.isika.cdi5.impairs;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class NombreImpairs {

	public List<Integer> genererNombresImpairsInferieurs(int nombreEntree) {
		if (estNegatifOuNul(nombreEntree))
			return Collections.emptyList();

		// Nouveau code qui génère une liste d'entiers partant de 1 à "nombreEntree"
		// Le 1 sera inclus et le nombreEntree exclus
		// Voir documentation Java de IntStream.range(......)
		// -> boxed() appelé permet de forcer les valeurs de int à Integer
		// -> filter permet de récupèrer uniquement les nombres impairs
		// -> collect(Collectors.toList()) permet de transformer le stream en liste
		List<Integer> listeNombresImpairs = IntStream.range(1, nombreEntree)
				.boxed()
				.filter(entier -> estImpair(entier))
				.collect(Collectors.toList());
		
		// Ancien code qui génère une liste d'entiers partant de 1 à "nombreEntree"
		/*List<Integer> listeNombresImpairs = new ArrayList<Integer>();
		for (int etape = 1; etape < nombreEntree; etape++) {
			if (estImpair(etape)) {
				listeNombresImpairs.add(etape);
			}
		}*/
		return listeNombresImpairs;
	}

	private boolean estImpair(int nombre) {
		return nombre % 2 == 1 ? true : false;
	}

	private boolean estNegatifOuNul(int nombre) {
		return nombre <= 0 ? true : false;
	}
}
