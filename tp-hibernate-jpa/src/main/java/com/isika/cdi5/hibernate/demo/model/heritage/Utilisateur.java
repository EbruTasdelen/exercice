package com.isika.cdi5.hibernate.demo.model.heritage;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

//@formatter:off

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE_UTILISATEUR", discriminatorType = DiscriminatorType.STRING)
@NamedQueries({
	@NamedQuery(name = "Utilisateur.findAll", query = "SELECT u FROM Utilisateur u")
})
//@formatter:on
public class Utilisateur {

	// Champs protected accessibles depuis les classes filles

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected Long id;

	@Column(name = "NOM")
	protected String nom;

	@Column(name = "PRENOM")
	protected String prenom;

	// On met insert et update à false car hibernate va gérer l'héritage lui même et
	// mettra les valeurs qu'il faut
//	@Column(name = "TYPE_UTILISATEUR", insertable = false, updatable = false)
//	protected String typeUtilisateur;

	public Utilisateur() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

//	public String getTypeUtilisateur() {
//		return typeUtilisateur;
//	}
//
//	public void setTypeUtilisateur(String typeUtilisateur) {
//		this.typeUtilisateur = typeUtilisateur;
//	}
}
