package com.isika.cdi5.hibernate.demo.services;

import java.util.Optional;

import com.isika.cdi5.hibernate.demo.model.personnes.Adresse;
import com.isika.cdi5.hibernate.demo.model.personnes.Personne;
import com.isika.cdi5.hibernate.demo.repo.PersonneRepository;

public class ServicePersonnes {

	private PersonneRepository personneRepository = new PersonneRepository();
	
	public Optional<Personne> creerPersonneDeTest() {
		Adresse adresse = new Adresse();
		adresse.setRue("1 Rue de la Gare");
		adresse.setComplement("Bat A");
		adresse.setCodePostal(75001);
		adresse.setVille("Paris");
		
		Personne personne = new Personne();
		personne.setNom("DURAND");
		personne.setPrenom("Test");
		personne.setAdresse(adresse);
		
		Long id = personneRepository.persister(personne);
		return personneRepository.findById(id);
	}

}
