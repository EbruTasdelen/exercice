package com.isika.cdi5.hibernate.demo.repo;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.isika.cdi5.hibernate.demo.model.articles.Article;
import com.isika.cdi5.hibernate.demo.model.personnes.Personne;
import com.isika.cdi5.hibernate.demo.utils.HibernateUtil;

public class PersonneRepository {

	private EntityManager entityManager;

	public PersonneRepository() {
		this.entityManager = HibernateUtil.createEntityManager();
	}

	public Long persister(Personne personne) {
		EntityTransaction transaction = this.entityManager.getTransaction();
		transaction.begin();
		this.entityManager.persist(personne);
		transaction.commit();
		return personne.getId();
	}

	public Optional<Article> rechercheParId(Long identifiantPersonne) {
		return Optional.ofNullable(entityManager.find(Article.class, identifiantPersonne));
	}

	public void supprimer(Personne personne) {
		EntityTransaction transaction = this.entityManager.getTransaction();
		transaction.begin();
		entityManager.remove(personne);
		transaction.commit();
	}

	public Personne modifier(Personne personne) {
		EntityTransaction transaction = this.entityManager.getTransaction();
		transaction.begin();
		Personne merged = entityManager.merge(personne);
		transaction.commit();
		return merged;
	}

	public Optional<Personne> findById(Long idPersonne) {
		return Optional.ofNullable(this.entityManager.find(Personne.class, idPersonne));
	}

	public List<Personne> findAll() {
		return this.entityManager.createQuery("SELECT p FROM Personne", Personne.class).getResultList();
	}

}
