package com.isika.cdi5.hibernate.demo.services;

import java.util.Optional;

import com.isika.cdi5.hibernate.demo.model.projets.Module;
import com.isika.cdi5.hibernate.demo.model.projets.Projet;
import com.isika.cdi5.hibernate.demo.repo.ProjetRepository;

public class ServiceProjets {
	
	private ProjetRepository projetRepository = new ProjetRepository();

	public Optional<Projet> creerProjetDeTest() {
		Projet projet = new Projet();
		projet.setNomProjet("TpHibernateJPA");

		Module moduleJpa = new Module();
		moduleJpa.setNom("JPA");
		moduleJpa.setDescription("Persistance de données avec JPA");
		
		Module moduleHsqldb = new Module();
		moduleHsqldb.setNom("Hsqldb");
		moduleHsqldb.setDescription("Base de données légère HSQLDB");
		projet.addModule(moduleJpa);
		projet.addModule(moduleHsqldb);
		
		Long idProjet = projetRepository.persister(projet);
		return projetRepository.rechercheParId(idProjet);
	}

}
