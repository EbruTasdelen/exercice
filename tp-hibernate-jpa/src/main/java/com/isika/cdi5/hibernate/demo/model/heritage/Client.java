package com.isika.cdi5.hibernate.demo.model.heritage;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "Client")
public class Client extends Utilisateur {

	@Column(name = "REFERENCE")
	private String referenceClient;

	public Client() {
		String tempRef = UUID.randomUUID().toString().substring(0, 10);
		this.referenceClient = tempRef;
	}

	public String getReferenceClient() {
		return referenceClient;
	}

	public void setReferenceClient(String referenceClient) {
		this.referenceClient = referenceClient;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Client [id=");
		builder.append(id);
		builder.append(", nom=");
		builder.append(nom);
		builder.append(", prenom=");
		builder.append(prenom);
		builder.append(", referenceClient=");
		builder.append(referenceClient);
		builder.append("]");
		return builder.toString();
	}
	
	
}
