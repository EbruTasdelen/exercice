package com.isika.cdi5.hibernate.demo.repo;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.isika.cdi5.hibernate.demo.model.projets.Projet;
import com.isika.cdi5.hibernate.demo.utils.HibernateUtil;

public class ProjetRepository {

	private EntityManager entityManager;

	public ProjetRepository() {
		this.entityManager = HibernateUtil.createEntityManager();
	}

	public Long persister(Projet projet) {
		EntityTransaction transaction = this.entityManager.getTransaction();
		transaction.begin();
		this.entityManager.persist(projet);
		
		projet.getModules().stream().forEach(module -> entityManager.persist(module));
		
		this.entityManager.flush();
		transaction.commit();
		return projet.getId();
	}

	public Optional<Projet> rechercheParId(Long identifiant) {
		return Optional.ofNullable(entityManager.find(Projet.class, identifiant));
	}

	public void supprimer(Projet projet) {
		EntityTransaction transaction = this.entityManager.getTransaction();
		transaction.begin();
		entityManager.remove(projet);
		transaction.commit();
	}

	public Projet modifier(Projet projet) {
		EntityTransaction transaction = this.entityManager.getTransaction();
		transaction.begin();
		Projet mergedProjet = entityManager.merge(projet);
		transaction.commit();
		return mergedProjet;
	}

	/**
	 * On appelle la méthode close pour fermer l'entity manager mais uniquement à la
	 * fin de l'application car c'est en mode "standalone"
	 */
	public void closeEntityManager() {
		entityManager.close();
	}
}
