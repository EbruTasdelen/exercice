package com.isika.cdi5.hibernate.demo.main;

import java.util.List;
import java.util.Optional;

import com.isika.cdi5.hibernate.demo.model.articles.Article;
import com.isika.cdi5.hibernate.demo.model.articles.Categorie;
import com.isika.cdi5.hibernate.demo.model.personnes.Personne;
import com.isika.cdi5.hibernate.demo.model.projets.Projet;
import com.isika.cdi5.hibernate.demo.services.ServiceArticles;
import com.isika.cdi5.hibernate.demo.services.ServicePersonnes;
import com.isika.cdi5.hibernate.demo.services.ServiceProjets;
import com.isika.cdi5.hibernate.demo.utils.HibernateUtil;

public class MainTpHibernateJpa {

	private static ServiceArticles serviceArticles = new ServiceArticles();
	private static ServicePersonnes servicePersonnes = new ServicePersonnes();
	private static ServiceProjets serviceProjets = new ServiceProjets();

	public static void main(String[] args) {

		// Articles
		System.out.println("**** Test Mapping Simple : Article *******");
		System.out.println();
		Optional<Article> optionalArticle = serviceArticles.creerArticleDeTest();
		optionalArticle.ifPresent(article -> System.out.println("Article persisté \n\t" + article));

		System.out.println("Liste de tous les articles");
		List<Article> tousLesArticles = serviceArticles.rechercheTousLesArticles();
		long count = tousLesArticles.parallelStream().count();
		System.out.println(String.format("\tIl y'a : %d article(s)", count));

		System.out.println("Recherche des articles de la catégorie : " + Categorie.JEUX);
		List<Article> articlesJeux = serviceArticles.rechercheArticleParCategorie(Categorie.JEUX);
		articlesJeux.stream().forEach(article -> System.out.println("\t" + article));

		// Relation 1-1 Bidirectionnelle : Une personne réside à une adresse,
		// une adresse appartient à une personne
		System.out.println();
		System.out.println("**** Test Mapping Relation 1-1 Bidirectionnelle : Personne <-> Adresse *******");
		System.out.println();
		Optional<Personne> optionalPersonne = servicePersonnes.creerPersonneDeTest();
		optionalPersonne.ifPresent(personne -> {
			System.out.println("Personne persistée \n\t" + personne);
		});

		// Relation 1-n Bidirectionnelle : Un projet possède plusieurs modules,
		// un module appartient à un projet
		System.out.println();
		System.out.println("**** Test Mapping Relation 1-n Bidirectionnelle : Projet <-> (n) Modules *******");
		System.out.println();
		Optional<Projet> optionalProjet = serviceProjets.creerProjetDeTest();
		optionalProjet.ifPresent(projet -> {
			System.out.println(String.format("Projet créé avec : %d module(s) - %s",
					projet.getModules().size(), projet));
			projet.getModules().stream().forEach(module -> System.out.println(module));
		});
		
		// Héritage
		System.out.println();
		System.out.println("**** Test Mapping Héritage SINGLE TABLE *******");
		System.out.println();
		HeritageTest heritageTest = new HeritageTest();
		heritageTest.creerHierarchieDeTest();
		
		// Arrêt de l'entity manager
		HibernateUtil.closeAll();
		// On arrête l'application (à ne pas faire dans une vraie application)
		System.exit(0);
	}

}
