package com.isika.cdi5.hibernate.demo.main;

import java.util.Optional;
import java.util.UUID;

import com.isika.cdi5.hibernate.demo.model.heritage.Client;
import com.isika.cdi5.hibernate.demo.model.heritage.Conseiller;
import com.isika.cdi5.hibernate.demo.model.heritage.Utilisateur;
import com.isika.cdi5.hibernate.demo.repo.UtilisateurRepository;

public class HeritageTest {

	private UtilisateurRepository repo = new UtilisateurRepository();
	
	public void creerHierarchieDeTest() {
		
		// Création d'un client
		Client premierClient = new Client();
		premierClient.setReferenceClient(UUID.randomUUID().toString());
		premierClient.setNom("DUPONT");
		premierClient.setPrenom("Pierre");
		Optional<Long> optId = repo.persister(premierClient);
		optId.ifPresent(id -> {
			Optional<Utilisateur> client = repo.rechercheParId(id);
			System.out.println("Client enregistré : " + client.get());
		});
		
		Conseiller conseiller = new Conseiller();
		conseiller.setAgence("PARIS");
		conseiller.setNom("DURAND");
		conseiller.setPrenom("Marie");
		Optional<Long> optIdCons = repo.persister(conseiller);
		optIdCons.ifPresent(idCons -> {
			Optional<Utilisateur> cons = repo.rechercheParId(idCons);
			System.out.println("Conseiller enregistré : " + cons.get());
		});
		
		// Affichage de toutes les entités (sans distinction de type)
		System.out.println();
		System.out.println("******************************");
		System.out.println("All utilisateurs (méthode rechercherToutesLesEntites dans le repo, utilisation de la classe mère)");
		System.out.println("******************************");
		repo.rechercherToutesLesEntites()
				.stream()
				.forEach(user -> System.out.println(user));
		
		// Utilisation de méthodes différentes pour chaque type
		System.out.println();
		System.out.println("******************************");
		System.out.println("Affichage des Clients (méthode findAllClients dans le repo)");
		System.out.println("******************************");
		repo.findAllClients().stream().forEach(client -> System.out.println(client));
		
		System.out.println();
		System.out.println("******************************");
		System.out.println("Affichage des Conseiller (méthode findAllConseillers dans le repo)");
		System.out.println("******************************");
		repo.findAllConseillers().stream().forEach(cons -> System.out.println(cons));
	}
	
}
