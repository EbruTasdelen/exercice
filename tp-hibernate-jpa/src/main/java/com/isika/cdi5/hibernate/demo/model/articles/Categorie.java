package com.isika.cdi5.hibernate.demo.model.articles;

public enum Categorie {

	// @formatter:off
	
	JEUX, 
	LIVRES, 
	MUSIQUE

	// @formatter:on
}
