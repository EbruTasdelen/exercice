package com.isika.cdi5.hibernate.demo.model.heritage;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "Conseiller")
public class Conseiller extends Utilisateur {

	@Column(name = "AGENCE")
	private String agence;

	public Conseiller() {
	}

	public String getAgence() {
		return agence;
	}
	public void setAgence(String agence) {
		this.agence = agence;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Conseiller [agence=");
		builder.append(agence);
		builder.append(", id=");
		builder.append(id);
		builder.append(", nom=");
		builder.append(nom);
		builder.append(", prenom=");
		builder.append(prenom);
		builder.append("]");
		return builder.toString();
	}
	
	
}
