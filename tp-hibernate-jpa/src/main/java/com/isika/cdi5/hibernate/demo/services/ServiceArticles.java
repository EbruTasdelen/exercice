package com.isika.cdi5.hibernate.demo.services;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.isika.cdi5.hibernate.demo.model.articles.Article;
import com.isika.cdi5.hibernate.demo.model.articles.Categorie;
import com.isika.cdi5.hibernate.demo.repo.ArticleRepository;

public class ServiceArticles {

	private ArticleRepository repository = new ArticleRepository();

	public Optional<Article> creerArticleDeTest() {
		
		Article jeuSteam = new Article();
		jeuSteam.setIntitule("The Long Dark");
		jeuSteam.setDateValidite(createDate());
		jeuSteam.setDescription("Description : Jeu de survie dans le nord glacial du Canada");
		jeuSteam.setQuantite(1);
		jeuSteam.setDisponibe(true);
		jeuSteam.setCategorie(Categorie.JEUX);
		
		Long id = repository.persister(jeuSteam);
		return rechercheArticleParId(id);
	}

	public Optional<Article> rechercheArticleParId(Long id) {
		return repository.findById(id);
	}

	public List<Article> rechercheTousLesArticles() {
		return repository.findAll();
	}

	public List<Article> rechercheArticleParCategorie(Categorie categorie) {
		return repository.findByCategorie(categorie);
	}
	
	private Date createDate() {
		// @formatter:off
		
		// Mieux que de faire new Date()
		
		return Date.from(
		
				// Date du jour au format LocalDate
				LocalDate.now()
				
				// Opérations de convertion au timezone par défaut
					.atStartOfDay()
					.atZone(ZoneId.systemDefault())
					
				// Changement de type de LocalDate à Instant
					.toInstant()
				);
		// @formatter:on
	}
	
}
