package com.isika.cdi5.hibernate.demo.repo;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.isika.cdi5.hibernate.demo.model.articles.Article;
import com.isika.cdi5.hibernate.demo.model.articles.Categorie;
import com.isika.cdi5.hibernate.demo.utils.HibernateUtil;

public class ArticleRepository {

	private EntityManager entityManager;

	public ArticleRepository() {
		this.entityManager = HibernateUtil.createEntityManager();
	}

	public Long persister(Article article) {
		EntityTransaction transaction = this.entityManager.getTransaction();
		transaction.begin();
		this.entityManager.persist(article);
		transaction.commit();
		return article.getIdentifiant();
	}

	public Optional<Article> rechercheParId(Long identifiantArticle) {
		return Optional.ofNullable(entityManager.find(Article.class, identifiantArticle));
	}

	public void supprimerArticle(Article article) {
		EntityTransaction transaction = this.entityManager.getTransaction();
		transaction.begin();
		entityManager.remove(article);
		transaction.commit();
	}

	public Article modifierArticle(Article article) {
		EntityTransaction transaction = this.entityManager.getTransaction();
		transaction.begin();
		Article mergedArticle = entityManager.merge(article);
		transaction.commit();
		return mergedArticle;
	}

	public Optional<Article> findById(Long idArticle) {
		// @formatter:off
		return Optional.ofNullable(
				this.entityManager.createNamedQuery("Article.findById", Article.class)
					.setParameter("idArticleParam", idArticle)
					.getSingleResult());
		// @formatter:on
	}

	public List<Article> findAll() {
		// @formatter:off
		
		return this.entityManager
				.createNamedQuery("Article.findAll", Article.class)
				.getResultList();
		
		// @formatter:on
	}

	public List<Article> findByCategorie(Categorie categorie) {
		// @formatter:off
		
		return this.entityManager
					.createQuery("SELECT art from Article art WHERE art.categorie =:categorieParam", Article.class)
					.setParameter("categorieParam", categorie)
					.getResultList();

		// @formatter:on
	}

	/**
	 * On appelle la méthode close pour fermer l'entity manager mais uniquement à la
	 * fin de l'application car c'est en mode "standalone"
	 */
	public void closeEntityManager() {
		entityManager.close();
	}
	
	public EntityManager getEntityManager() {
		return entityManager;
	}
}
