package com.isika.cdi5.hibernate.demo.model.projets;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Projet {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nomProjet;

	@OneToMany(mappedBy = "projet", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<Module> modules;

	public Projet() {
		this.modules = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public String getNomProjet() {
		return nomProjet;
	}

	public void setNomProjet(String nomProjet) {
		this.nomProjet = nomProjet;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void addModule(Module module) {
		module.setProjet(this);
		this.modules.add(module);
	}
	
	public void removeModule(Module module) {
		module.setProjet(null);
		this.modules.remove(module);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Projet [id=");
		builder.append(id);
		builder.append(", nomProjet=");
		builder.append(nomProjet);
		builder.append("]");
		return builder.toString();
	}
	
}
