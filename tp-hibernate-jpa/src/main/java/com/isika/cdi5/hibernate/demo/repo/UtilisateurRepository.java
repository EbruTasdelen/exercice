package com.isika.cdi5.hibernate.demo.repo;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.isika.cdi5.hibernate.demo.model.heritage.Client;
import com.isika.cdi5.hibernate.demo.model.heritage.Conseiller;
import com.isika.cdi5.hibernate.demo.model.heritage.Utilisateur;
import com.isika.cdi5.hibernate.demo.utils.HibernateUtil;

public class UtilisateurRepository {

	private EntityManager entityManager;

	public UtilisateurRepository() {
		entityManager = HibernateUtil.createEntityManager();
	}

	public List<Client> findAllClients() {
		return entityManager.createQuery("SELECT c FROM Client c", Client.class).getResultList();
	}

	public List<Conseiller> findAllConseillers() {
		return entityManager.createQuery("SELECT c FROM Conseiller c", Conseiller.class).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Utilisateur> rechercherToutesLesEntites() {
		return (List<Utilisateur>) entityManager.createNamedQuery("Utilisateur.findAll")
				.getResultList();
	}

	public Optional<Long> persister(Utilisateur entite) {
		EntityTransaction transaction = this.entityManager.getTransaction();
		transaction.begin();
		entityManager.persist(entite);
		entityManager.flush();
		transaction.commit();
		return Optional.ofNullable(entite.getId());
	}

	public Optional<Utilisateur> fusionner(Utilisateur entite) {
		EntityTransaction transaction = this.entityManager.getTransaction();
		transaction.begin();
		Utilisateur utilisateur = entityManager.merge(entite);
		entityManager.flush();
		transaction.commit();
		return Optional.ofNullable(utilisateur);
	}

	public void supprimer(Utilisateur entite) {
		EntityTransaction transaction = this.entityManager.getTransaction();
		transaction.begin();
		entityManager.remove(entite);
		entityManager.flush();
		transaction.commit();
	}

	public Optional<Utilisateur> rechercheParId(Long id) {
		return Optional.ofNullable((Utilisateur) entityManager.find(Utilisateur.class, id));
	}

}
