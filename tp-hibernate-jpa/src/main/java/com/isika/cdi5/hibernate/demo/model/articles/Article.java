package com.isika.cdi5.hibernate.demo.model.articles;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

// @formatter:off
@NamedQueries({
	@NamedQuery(name = "Article.findAll", query="SELECT art from Article art"),
	@NamedQuery(name = "Article.findById", query="SELECT art from Article art WHERE art.id= :idArticleParam")
})
//@formatter:on
@Entity
public class Article {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long identifiant;

	private String intitule;

	@Column(length = 512)
	private String description;

	@Temporal(TemporalType.DATE)
	private Date dateValidite;

	private Boolean disponibe;

	private Integer quantite;

	@Enumerated(EnumType.STRING)
	@Column(length = 15)
	private Categorie categorie;

	public Article() {
	}

	public Long getIdentifiant() {
		return identifiant;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateValidite() {
		return dateValidite;
	}

	public void setDateValidite(Date dateValidite) {
		this.dateValidite = dateValidite;
	}

	public Boolean isDisponibe() {
		return disponibe;
	}

	public void setDisponibe(Boolean disponibe) {
		this.disponibe = disponibe;
	}

	public Integer getQuantite() {
		return quantite;
	}

	public void setQuantite(Integer quantite) {
		this.quantite = quantite;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Article [identifiant=");
		builder.append(identifiant);
		builder.append(", intitule=");
		builder.append(intitule);
		builder.append(", description=");
		builder.append(description);
		builder.append(", dateValidite=");
		builder.append(dateValidite);
		builder.append(", disponibe=");
		builder.append(disponibe);
		builder.append(", quantite=");
		builder.append(quantite);
		builder.append(", categorie=");
		builder.append(categorie);
		builder.append("]");
		return builder.toString();
	}

}
