package com.isika.cdi5.java.jdbc.main.database.connection;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

public class JdbcDatabaseConnection {

	private static final Logger LOGGER = Logger.getLogger(JdbcDatabaseConnection.class.getSimpleName());
	private static final String DRIVER_NAME = "org.h2.Driver";
	
	private static final String SCHEMA_TABLES_SQL = "SELECT * FROM INFORMATION_SCHEMA.TABLES";
	private static final String JDBC_TEST_DATA_TABLES = "SELECT * FROM Personne";
	private String connectionString;
	private String userName;
	private String password;
	
	public JdbcDatabaseConnection(String connectionString, String userName, String password) {
		this.connectionString = connectionString;
		this.userName = userName;
		this.password = password;
		init();
	}

	public void connectToDatabase() {
		LOGGER.info("Ouverture d'une connexion à la base de données");
		try(Connection connection = DriverManager.getConnection(connectionString, userName, password)) {
			
			LOGGER.info("Fermeture de la connexion à la base de données");
			connection.close();
		} catch (SQLException e) {
			LOGGER.severe(String.format(
					"Erreur de connexion à la base de données avec l'url : %s et les identifiants (user : %s, password : %s)",
					connectionString, userName, password));
		}
	}
	
	public void printDatabaseData() {
		LOGGER.info("Affichage de données");
		try(Connection connection = DriverManager.getConnection(connectionString, userName, password)) {
			printSystemDatabaseInformation(connection);
			printTestTableContent(connection);
			
			LOGGER.info("Fermeture de la connexion à la base de données");
			connection.close();
		} catch (SQLException e) {
			LOGGER.severe(String.format(
					"Erreur de connexion à la base de données avec l'url : %s et les identifiants (user : %s, password : %s)",
					connectionString, userName, password));
		}
	}
	
	private void printSystemDatabaseInformation(Connection connection) throws SQLException {
		System.out.println();
		System.out.println("**** Affichage du contenu de la table système : INFORMATION_SCHEMA.TABLES ****");
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(SCHEMA_TABLES_SQL);
		while(resultSet.next()) {
			System.out.println(String.format("Catalog : %s, Schema : %s, TableName : %s, TableType : %s", 
					resultSet.getString("TABLE_CATALOG"),
					resultSet.getString("TABLE_SCHEMA"),
					resultSet.getString("TABLE_NAME"),
					resultSet.getString("TABLE_TYPE")));
		}
		System.out.println();
	}
	
	private void printTestTableContent(Connection connection) throws SQLException {
		System.out.println();
		System.out.println("**** Affichage du contenu de la table user : Personne ****");
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(JDBC_TEST_DATA_TABLES);
		while(resultSet.next()) {
			System.out.println(String.format("Id : %s, Nom : %s, Prénom : %s",
					resultSet.getString("IDPERSONNE"),
					resultSet.getString("NOMPERSONNE"),
					resultSet.getString("PRENOMPERSONNE")));
		}
		System.out.println();
	}
	
	private void init() {
		try {
			Class<?> driverClass = Class.forName(DRIVER_NAME);
			LOGGER.info("Pilote Jdbc Chargé avec succès : H2 Database");

			Driver jdbcDriver = (Driver) driverClass.newInstance();
			LOGGER.info("Instance du pilote Jdbc créée avec succès");

			DriverManager.registerDriver(jdbcDriver);
			LOGGER.info("Enregistrement de l'instance du pilote auprès du DriverManager avec susccès");
		} catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException e) {
			LOGGER.severe("Erreur de chargement du pilote de la base H2 - Message : " + e);
		}
	}
}
