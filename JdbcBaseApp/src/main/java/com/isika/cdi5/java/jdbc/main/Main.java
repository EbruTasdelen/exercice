package com.isika.cdi5.java.jdbc.main;

import com.isika.cdi5.java.jdbc.main.database.connection.JdbcDatabaseConnection;

public class Main {

	private static String connectionString = "jdbc:h2:D:\\DATABASE\\DATA\\MYDB";
	private static String userName = "sa";
	private static String password = "";
	private static JdbcDatabaseConnection databaseConnection = new JdbcDatabaseConnection(connectionString, userName,
			password);

	public static void main(String[] args) {
		//databaseConnection.connectToDatabase();
		databaseConnection.printDatabaseData();
	}

}
